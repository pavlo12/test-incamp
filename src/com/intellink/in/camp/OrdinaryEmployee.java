package com.intellink.in.camp;

/** Клас який описує робітників компанії
 * @author Pavlo Kurpela
 * @version 1.0
 */
public class OrdinaryEmployee extends Employee {

    private static final byte MAX_RANK = 6;

    byte rankEmployee;

    /**
     * Створує нового робітника
     * @param firstName ім'я
     * @param lastName прізвище
     * @param age вік
     * @param monthlyRate місячний оклад
     * @param rankEmployee розряд робітника
     */
    public OrdinaryEmployee(String firstName, String lastName, int age, double monthlyRate, byte rankEmployee) {
        super(firstName, lastName, age, monthlyRate);
        this.rankEmployee = rankEmployee;
    }

    /**
     * Створує пустий обєкт типу OrdinaryEmployee
     */
    public OrdinaryEmployee() {
    }

    public byte getRankEmployee() {
        return rankEmployee;
    }

    /**
     * Метод який дозволяє збільшити розряд робітника на один рівень
     */
    public void increaseRankEmployee() {
        if (rankEmployee <= MAX_RANK ) {
            rankEmployee = rankEmployee++;
        }

    }

    /**
     * Метод для оримання загальної заробітньої плати робітника за місяць
     * @return Заробітня плата
     */
    @Override
    public double salaryOfMonth() {
        double result = monthlyRate;
        switch (rankEmployee) {
            case 1: result = result;
            break;
            case 2: result = result + 250;
            break;

            case 3: result = result + 350;
            break;
            case 4: result = result + 500;
            break;
            case 5: result = result + 700;
            break;
            case 6: result = result + 1000;
            break;
        }
        return result;
    }
}
