package com.intellink.in.camp;

/** Клас який описує працівників компанії
 * @author Pavlo Kurpela
 * @version 1.0
 */
public abstract class Employee {
    private String firstName;
    private String lastName;
    private int age;
    double monthlyRate;  //місячний оклад для ппрацівника

    /**
     * Створує пустий обєкт типу Employee
     * @see Employee#Employee(String, String, int, double)
     */
    public Employee() {
    }

    /**
     * Створує нового працівника
     * @param firstName ім'я
     * @param lastName прізвище
     * @param age вік
     * @param monthlyRate місячний оклад
     */
    public Employee(String firstName, String lastName, int age, double monthlyRate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.monthlyRate = monthlyRate;
    }

    /**
     * Метод для задання імені працівника
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Метод для задання прізвища працівника
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Метод для отримання імені працівника
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Метод для отримання прізвища працівника
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Метод для отримання віку
     */
    public int getAge() {
        return age;
    }

    /**
     * Метод для задання віку працівника
     * @param age вік
     */
    public void setAge(int age) {
        if (age >= 16 && age <= 60) {
            this.age = age;
        }
        else {
            System.out.println("Вік працівника повинен бути від 16 до 60 років!!!");
        }
    }

    /**
     * Метод для розрахунку заробітньої плати працівника за місяціь
     * @return Заробітню плату працівника
     */
    public abstract double salaryOfMonth();
}
