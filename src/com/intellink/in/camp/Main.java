package com.intellink.in.camp;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();

        Manager manager = new Manager("Mykola", "Ptyts", 30, 5000);
        manager.setDateOFEmployment(25, 8, 2014);
        employees.add(manager);
        employees.add(new OrdinaryEmployee("Orest", "Bacinskiy", 25, 6500, (byte)2));
        manager = new Manager("Pavlo", "Kurpela", 24, 4500);
        manager.setDateOFEmployment(24, 01, 2016);
        employees.add(manager);
        OrdinaryEmployee ordinaryEmployee = new OrdinaryEmployee("Andriy", "Leskiv", 34, 5700, (byte)4);
        employees.add(ordinaryEmployee);
        manager = new Manager("Sasha", "Petrashchuk", 32, 7200);
        manager.setDateOFEmployment(25, 03, 2017);

        for (Employee e : employees) {
            System.out.println(e.salaryOfMonth());
        }

    }

}
