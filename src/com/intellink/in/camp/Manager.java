package com.intellink.in.camp;

import java.time.LocalDate;

        /** Клас який описує працівників компанії, які працюють на керівних посадах
        * @author Pavlo Kurpela
        * @version 1.0
        */
public class Manager extends Employee {

    private LocalDate dateOFEmployment;

    /**
     * Створує новий обєкт типу Manager
     * @param firstName ім'я
     * @param lastName прізвище
     * @param age вік
     * @param monthlyRate місячний оклад
     * @see Employee#Employee(String, String, int, double)
     */
    public Manager(String firstName, String lastName, int age, double monthlyRate) {
        super(firstName, lastName, age, monthlyRate);
    }

    /**
     * Створує пустий обєкт типу Employee
     * @see Manager#Manager(String, String, int, double)
     */
    public Manager() {
    }

    /**
     * Метод який обчислює стаж працівника у цій фірмі
     * @return стаж працівника
     */
    private int experience() {
        if (dateOFEmployment == null) return 0;
        int experience = LocalDate.now().getYear() - dateOFEmployment.getYear();
        if (LocalDate.now().getMonthValue() < dateOFEmployment.getMonthValue()) {
            experience--;
        }
        return experience;
    }

    /**
     * Метод який обчислює відсоток до зарплати для працівника
     * @return відсоток до зарплати
     */
    public  double percentageOfManagement() {
        double percentageOfManagement = 0.05;

        switch (experience()) {
            case 1: percentageOfManagement = 0.1;
            break;
            case 2: percentageOfManagement = 0.15;
            break;
            case 3: percentageOfManagement = 0.20;
            break;
            default: percentageOfManagement = 0.25;
        }
        return percentageOfManagement;
    }

    /**
     * Метод для введення дати прийняття на роботу
     * @param day число місяця
     * @param month місяць від 1 до 12
     * @param year рік
     */
    public void setDateOFEmployment(int day, int month, int year) {
        if (year > LocalDate.now().getYear()) return;
        if (year == LocalDate.now().getYear() && month > LocalDate.now().getMonthValue()) return;
        if (year == LocalDate.now().getYear() && month == LocalDate.now().getMonthValue() &&
                day > LocalDate.now().getDayOfMonth()) return;
        dateOFEmployment = LocalDate.of(year, month, day);
    }

    public LocalDate getDateOFEmployment() {
        return dateOFEmployment;
    }


    /**
     * Метод для оримання загальної заробітньої плати працівника за місяць
     * @return Заробітня плата
     */
    @Override
    public double salaryOfMonth() {
        double result;
        result = monthlyRate + monthlyRate * percentageOfManagement();
        return result;
    }
}
